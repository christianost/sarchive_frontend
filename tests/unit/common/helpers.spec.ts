import { filterGraphQlProperties } from '../../../src/common/helpers'

describe('helpers', () => {
  it('filterGraphQlProperties', () => {
    var testObject = {
      __typename: 'Testtype',
      test: '123'
    }
    var result = filterGraphQlProperties(testObject)
    expect(Object.keys(result).length).toEqual(1)
    expect(Object.keys(result).includes('__typename')).toBeFalsy()
  })
})
