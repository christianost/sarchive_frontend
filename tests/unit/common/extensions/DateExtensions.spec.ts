describe('DateExtensions', () => {
  it('toISODateString - Pads month and day up to two', () => {
    const date = new Date(2000, 1, 1)
    const dateString = date.toISODateString()
    expect(dateString).toEqual('2000-01-01')
  })

  it('toShortTimeString - Cuts off seconds and milliseconds', () => {
    const date = new Date(2000, 1, 1, 12, 30, 15, 123)
    const shortTimeString = date.toShortTimeString()
    expect(shortTimeString).toEqual('12:30')
  })
})
