import CaseFormatter from '../../../../src/common/formatters/CaseFormatter'

describe('CaseFormatter', () => {
  let testInstance: CaseFormatter

  beforeEach(() => {
    testInstance = new CaseFormatter()
  })

  it('first lower - then upper', () => {
    const result = testInstance.ensureFirstLetterUpper('test')
    expect(result).toEqual('Test')
  })

  it('first upper - stays upper', () => {
    const result = testInstance.ensureFirstLetterUpper('Test')
    expect(result).toEqual('Test')
  })
})
