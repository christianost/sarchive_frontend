import Cases from '../../../src/components/Cases/Cases.vue'
import { mount } from '@vue/test-utils'
import CaseFormInput from '../../../../src/models/CaseFormInput'
import { BoatType } from '../../../../src/global-query-types'
import {
  convertCaseFormInput,
  convertCaseToFormInput,
  determineTotalPob
} from '../../../../src/common/converters/CaseConverter'
import { CategoriesQuery_categories } from '../../../../src/components/Case/graphql/CategoriesQuery'
import { CasesQuery_cases } from '../../../../src/components/Cases/graphql/CasesQuery'

describe('CaseConverters', () => {
  describe('convertCaseFormInput', () => {
    var testInput: CaseFormInput
    var testCategories: CategoriesQuery_categories[]
    beforeEach(() => {
      testInput = <CaseFormInput>{
        boatColor: '#000000',
        boatType: BoatType.RUBBER,
        creationDate: '2020-01-01',
        creationTime: '12:00'
      }
      testCategories = []
    })

    it('Converts with default values without error', () => {
      convertCaseFormInput(testInput, testCategories)
    })

    it('links not added if null', () => {
      const result = convertCaseFormInput(testInput, testCategories)
      expect(result.case.links).toEqual([])
    })

    it('result valid - pob', () => {
      testInput.womenPob = 3
      const result = convertCaseFormInput(testInput, testCategories)
      expect(result.case!.peopleOnBoard!.women).toEqual(3)
    })

    it('result valid - tags, default category', () => {
      testInput.tags = ['test-tag1', 'test-tag2']
      const result = convertCaseFormInput(testInput, testCategories)
      expect(result.case!.categories![0]!.name).toEqual('others')
    })

    it('result valid - tags, first category', () => {
      testCategories = [<CategoriesQuery_categories>{ name: 'test-category' }]
      testInput.tags = ['test-tag1', 'test-tag2']
      const result = convertCaseFormInput(testInput, testCategories)
      expect(result.case!.categories![0]!.name).toEqual('test-category')
    })

    it('convertCaseToFormInput', () => {
      const inputCase = <CasesQuery_cases>{
        timestamp: '2020-01-01 12:12:12',
        freetext: 'this is free text',
        boat: {
          color: '#FFFFFF',
          engineStatus: 'broken',
          type: BoatType.METAL
        },
        peopleOnBoard: {
          women: 1,
          medical: 1,
          minors: 1,
          men: 1,
          drowned: 1,
          missing: 1
        },
        categories: [{ name: 'test-category', tags: [{ name: 'tag1' }] }]
      }

      const result = convertCaseToFormInput(inputCase)

      expect(result.creationDate).toEqual('2020-01-01')
      expect(result.creationTime).toEqual('12:12:12')
      expect(result.freetext).toEqual(inputCase.freetext)
      expect(result.boatColor).toEqual(inputCase.boat?.color)
      expect(result.medicalPob).toEqual(inputCase.peopleOnBoard?.medical)
      expect(result.tags![0]['value']).toEqual('tag1')
    })

    it('determineTotalPob', () => {
      const inputCase = <Partial<CasesQuery_cases>>{
        peopleOnBoard: {
          women: 1,
          medical: 1,
          minors: 1,
          men: 1,
          drowned: 1,
          missing: 1,
          total: 0
        }
      }

      const result = determineTotalPob(inputCase as CasesQuery_cases)
      expect(result).toEqual(3)

      inputCase.peopleOnBoard!.total = 5

      const result2 = determineTotalPob(inputCase as CasesQuery_cases)
      expect(result2).toEqual(5)
    })
  })
})
