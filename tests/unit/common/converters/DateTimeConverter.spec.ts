import {
  combineDateTime,
  dateToBackendString,
  backendStringToDateString,
  backendStringToTimeString
} from '../../../../src/common/converters/DateTimeConverter'

describe('combineDateTime', () => {
  it('Combine UI date time input', () => {
    const dateString = '2000-01-01'
    const timeString = '11:28'
    expect(combineDateTime(dateString, timeString)).toEqual(new Date('2000-01-01 11:28:00'))
  })
})

describe('dateToBackendString', () => {
  it('Transform to expected date/time format for BE timestamps', () => {
    var date = new Date(2000, 0, 1, 11, 28, 0, 0)
    expect(dateToBackendString(date)).toEqual('2000-01-01 11:28:00')
  })
})

describe('backendStringToDateString', () => {
  it('Transform from the backends date/time format to the UI date format', () => {
    const backendString = '2000-01-01 11:28:00'
    expect(backendStringToDateString(backendString)).toEqual('2000-01-01')
  })
})

describe('backendStringToTimeString', () => {
  it('Transform from the backends date/time format to the UI time format', () => {
    const backendString = '2000-01-01 11:28:00'
    expect(backendStringToTimeString(backendString)).toEqual('11:28:00')
  })
})
