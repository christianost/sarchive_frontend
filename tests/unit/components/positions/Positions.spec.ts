import Positions from '../../../../src/components/Positions/Positions.vue'
import { shallowMount } from '@vue/test-utils'

describe('Positions', () => {
  it('Renders without error', () => {
    const wrapper = shallowMount(Positions, { stubs: ['b-icon-backspace'] })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
