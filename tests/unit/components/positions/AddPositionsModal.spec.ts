import AddPositionModal from '../../../../src/components/Positions/AddPositionModal.vue'
import { shallowMount } from '@vue/test-utils'

describe('AddPositionModal', () => {
  it('Renders without error', () => {
    const wrapper = shallowMount(AddPositionModal, { stubs: ['input-field'] })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
