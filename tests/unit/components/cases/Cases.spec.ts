import Cases from '../../../../src/components/Cases/Cases.vue'
import { shallowMount } from '@vue/test-utils'
import serverBus from '../../../../src/common/serverBus'
import * as authentication from '../../../../src/common/authentication'
import * as caseQueries from '../../../../src/components/Cases/CasesQueries'
import { CaseQuery_case } from '../../../../src/components/Case/graphql/CaseQuery'
import {
  CasesQuery_cases_boat,
  CasesQuery_cases_peopleOnBoard,
  CasesQuery_cases
} from '../../../../src/components/Cases/graphql/CasesQuery'
import { BoatType } from '../../../../src/global-query-types'

let testCaseObjects = [
  <CasesQuery_cases>{
    __typename: 'Case',
    caseNumber: 'TEST',
    freetext: 'test',
    boat: <CasesQuery_cases_boat>{
      engineStatus: 'broken',
      color: 'red',
      type: BoatType.RUBBER
    },
    links: ['link1'],
    id: '1',
    positions: [],
    timestamp: '2020-01-01 12:12:12',
    peopleOnBoard: <CasesQuery_cases_peopleOnBoard>{
      drowned: 0,
      medical: 0,
      men: 1,
      women: 1,
      minors: 1,
      missing: 0
    },
    categories: []
  },
  <CasesQuery_cases>{
    __typename: 'Case',
    caseNumber: 'TEST',
    freetext: 'test',
    boat: <CasesQuery_cases_boat>{
      engineStatus: 'broken',
      color: 'red',
      type: BoatType.RUBBER
    },
    links: ['link1'],
    id: '2',
    positions: [],
    timestamp: '2020-01-01 12:12:12',
    peopleOnBoard: <CasesQuery_cases_peopleOnBoard>{
      drowned: 0,
      medical: 0,
      men: 1,
      women: 1,
      minors: 1,
      missing: 0
    },
    categories: []
  }
]

jest.mock('../../../../src/common/serverBus')

describe('Cases', () => {
  function render() {
    return shallowMount(Cases, {
      stubs: [
        'success-alert',
        'error-alert',
        'b-icon-download',
        'b-icon-plus',
        'b-icon-archive-fill'
      ]
    })
  }

  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('refresh cases populates cases', async () => {
    const wrapper = render()
    wrapper.find('#refreshCases').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$data.cases).toBeTruthy()
  })

  it('create registers serverBus', async () => {
    const _ = render()
    const busOn = serverBus.$on as jest.Mock<any, any>
    expect(busOn).toHaveBeenCalled()
    expect(busOn).toHaveBeenCalledWith('logged_in', expect.anything())
    expect(busOn).toHaveBeenCalledWith('logged_out', expect.anything())
    expect(busOn).toHaveBeenCalledWith('add_case_success', expect.anything())
    expect(busOn).toHaveBeenCalledWith('add_case_closed', expect.anything())
  })

  it('on mount loads cases', async () => {
    jest.spyOn(authentication, 'isAuthenticated').mockImplementation(() => true)
    const loadCasesMock = jest.fn()
    jest.spyOn(caseQueries, 'loadCases').mockImplementation(loadCasesMock)

    const wrapper = render()
    await wrapper.vm.$nextTick()

    expect(loadCasesMock).toBeCalledTimes(1)
  })

  it('error on loading cases - shows error', async () => {
    jest.spyOn(authentication, 'isAuthenticated').mockImplementation(() => true)
    const loadCasesMock = () => {
      throw 'test-error'
    }
    jest.spyOn(caseQueries, 'loadCases').mockImplementation(loadCasesMock)

    const wrapper = render()
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.$data.errorMessage).toEqual('test-error')
  })

  it('loads cases - displays items', async () => {
    jest.spyOn(authentication, 'isAuthenticated').mockImplementation(() => true)
    jest.spyOn(caseQueries, 'loadCases').mockImplementation(() => Promise.resolve(testCaseObjects))

    const wrapper = render()
    await wrapper.vm.$nextTick()

    expect(wrapper.find('.caseListItem')).toBeTruthy()
  })
})
