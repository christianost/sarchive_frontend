import Login from '../../../../src/components/Modals/Login.vue'
import { shallowMount } from '@vue/test-utils'

describe('Login', () => {
  const render = () => {
    return shallowMount(Login, {
      stubs: ['error-alert'],
      attachToDocument: true,
      propsData: {
        static: true
      }
    })
  }

  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  // TODO test modal login's login functionality, with the current version of bootstrap vue, the button is not part of the markup, hence it is not possible to test the button
})
