import Modals from '../../../../src/components/Modals/Modals.vue'
import { shallowMount } from '@vue/test-utils'

describe('Modals', () => {
  it('Renders without error', () => {
    const wrapper = shallowMount(Modals, { stubs: ['add-position'] })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
