import PositionListItem from '../../../../src/components/Case/PositionListItem.vue'
import { mount } from '@vue/test-utils'
import { CasesQuery_cases_positions } from '../../../../src/components/Cases/graphql/CasesQuery'
import { CaseQuery_case_positions } from '../../../../src/components/Case/graphql/CaseQuery'
import * as formatters from '../../../../src/common/formatters'

let testPosition: CasesQuery_cases_positions
let deleteHandlerMock: jest.Mock<any, any>

function render(position: CasesQuery_cases_positions = testPosition) {
  return mount(PositionListItem, {
    // we need to mount this component to test the button functionality
    stubs: ['b-icon-trash-fill'],
    propsData: {
      position: position,
      onClickDeletePosition: deleteHandlerMock
    }
  })
}

describe('PositionListItem', () => {
  beforeEach(() => {
    jest.spyOn(formatters, 'formatLatLng').mockImplementation((value: number) => value as any) // return value as is, we don't test the formatter here
    testPosition = <CaseQuery_case_positions>{
      id: '1',
      caseId: '1',
      latitude: 35.12233,
      longitude: 8.11111,
      timestamp: '2020-01-01 12:12:12'
    }
    deleteHandlerMock = jest.fn()
  })

  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('Mounted - displays position info', () => {
    const wrapper = render()
    expect(wrapper.find('.position-latitude').text()).toEqual('Lat: ' + testPosition.latitude)
    expect(wrapper.find('.position-longitude').text()).toEqual('Lng: ' + testPosition.longitude)
    expect(wrapper.find('.position-timestamp').text()).toEqual('At: ' + testPosition.timestamp)
  })

  it('Mounted - delete icon is variant secondary', () => {
    const wrapper = render()
    expect(wrapper.find('.btn-secondary').exists()).toBeTruthy()
  })

  it('Clicked - delete icon is variant danger', async () => {
    const wrapper = render()
    await wrapper.find('.btn-secondary').trigger('click')
    expect(wrapper.find('.btn-danger').exists()).toBeTruthy()
  })

  it('Clicked twice - delete handler is triggered', async () => {
    const wrapper = render()
    await wrapper.find('.btn-secondary').trigger('click')
    await wrapper.find('.btn-danger').trigger('click')

    expect(deleteHandlerMock).toBeCalledTimes(1)
  })
})
