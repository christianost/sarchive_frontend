import CaseDetail from '../../../../src/components/Case/CaseDetail.vue'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import { CaseQuery_case } from '../../../../src/components/Case/graphql/CaseQuery'
import { BoatType } from '../../../../src/global-query-types'
import {
  CasesQuery_cases_boat,
  CasesQuery_cases_peopleOnBoard
} from '../../../../src/components/Cases/graphql/CasesQuery'
import * as queries from '../../../../src/components/Case/Queries'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'

const localVue = createLocalVue()

let testCaseObject = <CaseQuery_case>{
  __typename: 'Case',
  caseNumber: 'TEST',
  freetext: 'test',
  boat: <CasesQuery_cases_boat>{
    engineStatus: 'broken',
    color: 'red',
    type: BoatType.RUBBER
  },
  links: ['link1'],
  id: '1',
  positions: [],
  timestamp: '2020-01-01 12:12:12',
  peopleOnBoard: <CasesQuery_cases_peopleOnBoard>{
    drowned: 0,
    medical: 0,
    men: 1,
    women: 1,
    minors: 1,
    missing: 0
  },
  categories: []
}

function render() {
  return shallowMount(CaseDetail, {
    localVue,
    mocks: {
      $route: {
        params: {
          id: 'test-id'
        }
      }
    }
  })
}

describe('CaseDetail', () => {
  beforeEach(() => {
    jest
      .spyOn(queries, 'loadCase')
      .mockImplementation((apollo: DollarApollo<any>, caseNumber: string) =>
        Promise.resolve(testCaseObject)
      )
  })

  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('Renders men + women + minors as total POB', async () => {
    const wrapper = render()
    await wrapper.vm.$nextTick()
    expect(wrapper.html().includes('3')).toBeTruthy()
  })
})
