import CaseDetailData from '../../../../src/components/Case/CaseDetailData.vue'
import { shallowMount } from '@vue/test-utils'
import {
  CaseQuery_case_peopleOnBoard,
  CaseQuery_case_boat,
  CaseQuery_case
} from '../../../../src/components/Case/graphql/CaseQuery'
import { BoatType } from '../../../../src/global-query-types'

let testCaseObject = <Partial<CaseQuery_case>>{
  caseNumber: 'TEST',
  freetext: 'test',
  boat: <Partial<CaseQuery_case_boat>>{
    engineStatus: 'broken',
    color: 'red',
    type: BoatType.RUBBER
  },
  links: ['link1'],
  id: '1',
  positions: [],
  timestamp: '2020-01-01 12:12:12',
  peopleOnBoard: <Partial<CaseQuery_case_peopleOnBoard>>{
    drowned: 0,
    medical: 0,
    men: 1,
    women: 1,
    minors: 1,
    missing: 0,
    total: 1
  },
  categories: []
}

function render() {
  return shallowMount(CaseDetailData, {
    propsData: {
      caseObject: testCaseObject
    },
    stubs: ['b-icon-plus']
  })
}

describe('CaseDetailData', () => {
  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
