import CaseForm from '../../../../src/components/Case/CaseForm.vue'
import { shallowMount } from '@vue/test-utils'
import CaseFormInput from '../../../../src/models/CaseFormInput'
import cuid from 'cuid'
import { BoatType } from '../../../../src/global-query-types'

const now = new Date()
const testCaseData = <CaseFormInput>{
  caseNumber: cuid(),
  boatType: BoatType.RUBBER,
  boatColor: '#FF0000',
  engineStatus: '',
  womenPob: 0,
  menPob: 0,
  minorsPob: 0,
  drownedPob: 0,
  missingPob: 0,
  medicalPob: 0,
  creationDate: now.toISODateString(),
  creationTime: now.toShortTimeString(),
  tags: []
}

function render() {
  return shallowMount(CaseForm, {
    propsData: { value: testCaseData },
    stubs: ['input-field', 'tags-input']
  })
}

describe('CaseForm', () => {
  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  const inputIds = [
    'caseNumber',
    'creation-date',
    'boat-type',
    'boat-color',
    'engine-status',
    'freetext',
    'pob-women',
    'pob-men',
    'pob-minors',
    'pob-medical',
    'pob-drowned',
    'pob-missing',
    'pob-total',
    'link'
  ]
  inputIds.forEach(inputId => {
    it('Verifying input for id "' + inputId, () => {
      const wrapper = render()
      expect(wrapper.html()).toContain(inputId)
    })
  })
})
