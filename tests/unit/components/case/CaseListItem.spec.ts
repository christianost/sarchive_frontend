import CaseListItem from '../../../../src/components/Case/CaseListItem.vue'
import { shallowMount } from '@vue/test-utils'
import { CaseQuery_case } from '../../../../src/components/Case/graphql/CaseQuery'
import {
  CasesQuery_cases_boat,
  CasesQuery_cases_peopleOnBoard
} from '../../../../src/components/Cases/graphql/CasesQuery'
import { BoatType } from '../../../../src/global-query-types'

function render() {
  return shallowMount(CaseListItem)
}

describe('CaseListItem', () => {
  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
