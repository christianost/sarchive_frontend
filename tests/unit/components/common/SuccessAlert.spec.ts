import SuccessAlert from '../../../../src/components/Common/SuccessAlert.vue'
import { shallowMount } from '@vue/test-utils'

describe('SuccessAlert', () => {
  it('Renders without error', () => {
    const wrapper = shallowMount(SuccessAlert)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
