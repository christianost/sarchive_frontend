import InputField from '../../../../src/components/Common/InputField.vue'
import { shallowMount } from '@vue/test-utils'

describe('InputField', () => {
  it('Renders without error', () => {
    const wrapper = shallowMount(InputField)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
