import MenuBar from '../../../../src/components/Common/MenuBar.vue'
import { shallowMount } from '@vue/test-utils'

describe('MenuBar', () => {
  it('Renders without error', () => {
    const wrapper = shallowMount(MenuBar, {
      stubs: ['b-icon-archive-fill', 'b-icon-plus', 'b-icon-search', 'b-icon-power']
    })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
