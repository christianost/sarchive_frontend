import ErrorAlert from '../../../../src/components/Common/ErrorAlert.vue'
import { shallowMount } from '@vue/test-utils'

describe('ErrorAlert', () => {
  it('Renders without error', () => {
    const wrapper = shallowMount(ErrorAlert)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
