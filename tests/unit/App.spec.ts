import App from '../../src/App.vue'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import serverBus from '../../src/common/serverBus'
import * as authentication from '../../src/common/authentication'

jest.mock('../../src/common/serverBus')

describe('App', () => {
  const render = (mocks: object = {}) => {
    // Use local vue instance to allow plugin mocking
    const localVue = createLocalVue()

    return shallowMount(App, {
      localVue,
      stubs: ['menu-bar', 'router-view', 'login', 'modals'],
      mocks: mocks
    })
  }

  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('create registers serverBus', () => {
    const _ = render()
    const busOn = serverBus.$on as jest.Mock<any, any>
    expect(busOn).toHaveBeenCalled()
    expect(busOn).toHaveBeenCalledWith('logged_in', expect.anything())
    expect(busOn).toHaveBeenCalledWith('logged_out', expect.anything())
    expect(busOn).toHaveBeenCalledWith('token_expired', expect.anything())
    expect(busOn).toHaveBeenCalledWith('error', expect.anything())
  })

  it('not logged in - show login', async () => {
    jest.spyOn(authentication, 'isAuthenticated').mockImplementation(() => false)

    const wrapper = render()
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.$data.showLoginModal).toBeTruthy()
  })

  it('logged in - dont show login', async () => {
    jest.spyOn(authentication, 'isAuthenticated').mockImplementation(() => true)

    const wrapper = render()
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.$data.showLoginModal).toBeFalsy()
  })
})
