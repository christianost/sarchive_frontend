import Vue from 'vue'
module.exports = {
  LMap: new Vue({ template: '<div>L_MAP</div>' }),
  LTileLayer: new Vue({ template: '<div>L_TILE_LAYER</div>' }),
  LMarker: new Vue({ template: '<div>L_MARKER</div>' })
}
