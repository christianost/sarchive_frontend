module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  transformIgnorePatterns: [
    '/node_modules/(?!register-service-worker|bootstrap|bootstrap-vue|vee-validate/dist/rules)'
  ],
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '^.+\\.ts?$': 'ts-jest',
    '^.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub'
  },
  collectCoverageFrom: [
    'src/**/*.{ts,vue}',
    '!**/node_modules/**',
    '!src/registerServiceWorker.ts',
    '!src/main.ts',
    '!src/common/graphqlClient.ts',
    '!src/common/logger.ts',
    '!src/common/storage.ts',
    '!src/router/index.ts'
  ],
  coverageThreshold: {
    global: {
      branches: 50,
      functions: 50,
      lines: 0,
      statements: 0
    }
  },
  coverageReporters: ['html', 'json'],
  setupFiles: ['./tests/unit/setup.ts'],
  moduleNameMapper: {
    '\\.(css|less)$': '<rootDir>/__mocks__/styleMock.ts',
    '\\.(png|jpg|jpeg)$': '<rootDir>/__mocks__/imageMock.ts'
  }
}
