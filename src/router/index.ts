import Vue from 'vue'
import VueRouter from 'vue-router'
import Cases from '../components/Cases/Cases.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'cases',
    component: Cases
  },
  {
    path: '/case/:id',
    name: 'caseDetail',
    component: () =>
      import(/* webpackChunkName: "positions" */ '../components/Case/CaseDetail.vue'),
    props: true
  },
  {
    path: '/case/:id/positions',
    name: 'positions',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "positions" */ '../components/Positions/Positions.vue'),
    props: true
  },
  {
    path: '/case/edit',
    name: 'editCase',
    component: () => import(/* webpackChunkName: "editCase" */ '../components/Case/EditCase.vue'),
    props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
