import Vue from 'vue'
import './registerServiceWorker'
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from './router'
import App from './App.vue'
import BootstrapVue, { BootstrapVueIcons } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueApollo from 'vue-apollo'
import './common/extensions/DateExtensions'
import { ValidationObserver, ValidationProvider } from 'vee-validate'
import './common/validations'
import InputField from './components/Common/InputField.vue'
import ErrorAlert from './components/Common/ErrorAlert.vue'
import SuccessAlert from './components/Common/SuccessAlert.vue'

import VoerroTagsInput from '@voerro/vue-tagsinput'
import '@voerro/vue-tagsinput/dist/style.css'

Vue.component('tags-input', VoerroTagsInput)

// Install VeeValidate components globally
Vue.component('validation-observer', ValidationObserver)
Vue.component('validation-provider', ValidationProvider)

// Install custom components globally
Vue.component('input-field', InputField)
Vue.component('error-alert', ErrorAlert)
Vue.component('success-alert', SuccessAlert)

Vue.config.productionTip = false
Vue.use(VueAxios, axios)
Vue.use(VueApollo)
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

import apolloClient from './common/graphqlClient'
const apolloProvider = new VueApollo({
  defaultClient: apolloClient
})
new Vue({
  router,
  apolloProvider,
  render: h => h(App)
}).$mount('#app')
