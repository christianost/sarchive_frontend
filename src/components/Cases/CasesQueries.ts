import gql from 'graphql-tag'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import { CasesQuery, CasesQuery_cases } from './graphql/CasesQuery'
import { FetchResult } from 'apollo-link'
import logger from '../../common/logger'

export const CASES_QUERY = gql`
  query CasesQuery {
    cases {
      id
      caseNumber
      freetext
      links
      timestamp
      boat {
        type
        color
        engineStatus
      }
      peopleOnBoard {
        women
        minors
        men
        medical
        missing
        drowned
        total
      }
      positions {
        id
        caseId
        latitude
        longitude
        timestamp
      }
      categories {
        name
        tags {
          name
        }
      }
    }
  }
`

export async function loadCases(
  apollo: DollarApollo<any>
): Promise<(CasesQuery_cases | null)[] | null> {
  const result = (await apollo.query<CasesQuery>({ query: CASES_QUERY })) as FetchResult<CasesQuery>
  const data = result.data as CasesQuery
  logger.trace(`Refreshed cases with result '${JSON.stringify(data)}'`)
  return data.cases
}
