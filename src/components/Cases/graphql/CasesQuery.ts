/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { BoatType } from "./../../../global-query-types";

// ====================================================
// GraphQL query operation: CasesQuery
// ====================================================

export interface CasesQuery_cases_boat {
  __typename: "Boat";
  type: BoatType | null;
  color: string | null;
  engineStatus: string | null;
}

export interface CasesQuery_cases_peopleOnBoard {
  __typename: "PeopleOnBoard";
  women: number | null;
  minors: number | null;
  men: number | null;
  medical: number | null;
  missing: number | null;
  drowned: number | null;
  total: number | null;
}

export interface CasesQuery_cases_positions {
  __typename: "Position";
  id: string;
  caseId: string;
  latitude: number;
  longitude: number;
  timestamp: string | null;
}

export interface CasesQuery_cases_categories_tags {
  __typename: "Tag";
  name: string;
}

export interface CasesQuery_cases_categories {
  __typename: "Category";
  name: string;
  tags: CasesQuery_cases_categories_tags[] | null;
}

export interface CasesQuery_cases {
  __typename: "Case";
  id: string;
  caseNumber: string;
  freetext: string | null;
  links: (string | null)[] | null;
  timestamp: string | null;
  boat: CasesQuery_cases_boat | null;
  peopleOnBoard: CasesQuery_cases_peopleOnBoard | null;
  positions: (CasesQuery_cases_positions | null)[] | null;
  categories: (CasesQuery_cases_categories | null)[] | null;
}

export interface CasesQuery {
  cases: (CasesQuery_cases | null)[] | null;
}
