import gql from 'graphql-tag'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import { PositionInput } from '../../global-query-types'
import { combineDateTimeToBackendString } from '../../common/converters/DateTimeConverter'
import logger from '../../common/logger'
import { AddPositionToCase, AddPositionToCaseVariables } from './graphql/AddPositionToCase'

export const ADD_POSITION_MUTATION = gql`
  mutation AddPositionToCase($caseId: ID!, $position: PositionInput!) {
    addPositionToCase(caseId: $caseId, position: $position) {
      latitude
      longitude
      timestamp
      caseId
    }
  }
`

export async function addPositionToCase(
  apollo: DollarApollo<any>,
  latitude: any,
  longitude: any,
  date: string,
  time: string,
  caseId: string
) {
  const position: PositionInput = {
    latitude: Number(latitude),
    longitude: Number(longitude),
    timestamp: combineDateTimeToBackendString(date, time)
  }
  logger.trace(`Adding postion with data '${JSON.stringify(position)}' to case with ID '${caseId}'`)
  await apollo.mutate<AddPositionToCase, AddPositionToCaseVariables>({
    mutation: ADD_POSITION_MUTATION,
    variables: {
      caseId: caseId,
      position: position
    }
  })
}
