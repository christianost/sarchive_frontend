/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PositionInput } from "./../../../global-query-types";

// ====================================================
// GraphQL mutation operation: AddPositionToCase
// ====================================================

export interface AddPositionToCase_addPositionToCase {
  __typename: "Position";
  latitude: number;
  longitude: number;
  timestamp: string | null;
  caseId: string;
}

export interface AddPositionToCase {
  /**
   * Cases can have one or many positions. Positions can be added or
   * removed from cases. They are sequentially numbered relative to
   * a case
   */
  addPositionToCase: AddPositionToCase_addPositionToCase | null;
}

export interface AddPositionToCaseVariables {
  caseId: string;
  position: PositionInput;
}
