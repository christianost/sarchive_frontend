import gql from 'graphql-tag'
import logger from '../../common/logger'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import { CategoriesQuery, CategoriesQuery_categories } from './graphql/CategoriesQuery'
import { loadCases } from '../Cases/CasesQueries'
import { CaseQuery, CaseQuery_case } from './graphql/CaseQuery'
import { FetchResult } from 'apollo-link'
import serverBus from '../../common/serverBus'
import {
  DeletePositionMutation,
  DeletePositionMutationVariables
} from './graphql/DeletePositionMutation'

export const CATEGORIES_QUERY = gql`
  query CategoriesQuery {
    categories {
      id
      name
      tags {
        name
      }
    }
  }
`

export const CREATE_CASE_MUTATION = gql`
  mutation CreateCaseMutation($case: CreateCaseInput!) {
    createCase(case: $case) {
      id
    }
  }
`

export const UPDATE_CASE_MUTATION = gql`
  mutation UpdateCaseMutation($case: UpdateCaseInput!) {
    updateCase(case: $case) {
      id
    }
  }
`

export async function refreshCategories(apollo: DollarApollo<any>) {
  try {
    const result = await apollo.query<CategoriesQuery>({ query: CATEGORIES_QUERY })
    logger.trace(`Retrieved existing tag categories: '${JSON.stringify(result)}'`)
    if (result != null && result.data.categories != null) {
      return result.data.categories.map((c: CategoriesQuery_categories | null) => {
        if (c != null) return c
      })
    }
  } catch (e) {
    logger.error(e)
  }
}

export const CASE_QUERY = gql`
  query CaseQuery($id: ID!) {
    case(id: $id) {
      id
      caseNumber
      freetext
      links
      timestamp
      boat {
        type
        color
        engineStatus
      }
      peopleOnBoard {
        women
        minors
        men
        medical
        missing
        drowned
        total
      }
      positions {
        id
        caseId
        latitude
        longitude
        timestamp
      }
      categories {
        name
        tags {
          name
        }
      }
    }
  }
`

export async function loadCase(
  apollo: DollarApollo<any>,
  caseNumber: string
): Promise<CaseQuery_case | undefined> {
  try {
    const result = (await apollo.query<CaseQuery>({
      query: CASE_QUERY,
      variables: { id: caseNumber }
    })) as FetchResult<CaseQuery>

    const data = result.data as CaseQuery
    logger.trace(`Loaded case with result '${JSON.stringify(data)}'`)

    if (data.case == null || data.case.boat == null || data.case.peopleOnBoard == null) {
      throw new Error('Incomplete case object')
    }
    return data.case
  } catch (e) {
    logger.error(e)
    serverBus.$emit('error', e)
  }
}

const DELETE_POSITION_MUTATION = gql`
  mutation DeletePositionMutation($id: ID!, $caseId: ID!) {
    deletePositionFromCase(id: $id, caseId: $caseId)
  }
`

export async function deletePosition(
  apollo: DollarApollo<any>,
  id: string,
  caseId: string
): Promise<boolean> {
  let success: boolean = false
  logger.trace(`Deleting position '${id}' of case '${caseId}'`)
  try {
    const result = (await apollo.mutate<DeletePositionMutation, DeletePositionMutationVariables>({
      mutation: DELETE_POSITION_MUTATION,
      variables: { id: id, caseId: caseId }
    })) as FetchResult<DeletePositionMutation>

    success = result.data != null && result.data!.deletePositionFromCase!
    logger.trace(`Deleted position '${id}' of case '${caseId}'. Success: '${success}'`)
  } catch (e) {
    logger.error(e)
    serverBus.$emit('error', e)
  }
  return success
}
