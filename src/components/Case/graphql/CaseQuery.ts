/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { BoatType } from "./../../../global-query-types";

// ====================================================
// GraphQL query operation: CaseQuery
// ====================================================

export interface CaseQuery_case_boat {
  __typename: "Boat";
  type: BoatType | null;
  color: string | null;
  engineStatus: string | null;
}

export interface CaseQuery_case_peopleOnBoard {
  __typename: "PeopleOnBoard";
  women: number | null;
  minors: number | null;
  men: number | null;
  medical: number | null;
  missing: number | null;
  drowned: number | null;
  total: number | null;
}

export interface CaseQuery_case_positions {
  __typename: "Position";
  id: string;
  caseId: string;
  latitude: number;
  longitude: number;
  timestamp: string | null;
}

export interface CaseQuery_case_categories_tags {
  __typename: "Tag";
  name: string;
}

export interface CaseQuery_case_categories {
  __typename: "Category";
  name: string;
  tags: CaseQuery_case_categories_tags[] | null;
}

export interface CaseQuery_case {
  __typename: "Case";
  id: string;
  caseNumber: string;
  freetext: string | null;
  links: (string | null)[] | null;
  timestamp: string | null;
  boat: CaseQuery_case_boat | null;
  peopleOnBoard: CaseQuery_case_peopleOnBoard | null;
  positions: (CaseQuery_case_positions | null)[] | null;
  categories: (CaseQuery_case_categories | null)[] | null;
}

export interface CaseQuery {
  case: CaseQuery_case | null;
}

export interface CaseQueryVariables {
  id: string;
}
