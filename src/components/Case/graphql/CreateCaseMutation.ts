/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CreateCaseInput } from "./../../../global-query-types";

// ====================================================
// GraphQL mutation operation: CreateCaseMutation
// ====================================================

export interface CreateCaseMutation_createCase {
  __typename: "Case";
  id: string;
}

export interface CreateCaseMutation {
  /**
   * Creates a new case. A unique case id has to be
   * specified. Calling this with an existing id will result
   * in an error. The API does not take care of generating
   * ids. That has to be handled by the caller.
   */
  createCase: CreateCaseMutation_createCase | null;
}

export interface CreateCaseMutationVariables {
  case: CreateCaseInput;
}
