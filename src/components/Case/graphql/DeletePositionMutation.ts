/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeletePositionMutation
// ====================================================

export interface DeletePositionMutation {
  deletePositionFromCase: boolean | null;
}

export interface DeletePositionMutationVariables {
  id: string;
  caseId: string;
}
