/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdateCaseInput } from "./../../../global-query-types";

// ====================================================
// GraphQL mutation operation: UpdateCaseMutation
// ====================================================

export interface UpdateCaseMutation_updateCase {
  __typename: "Case";
  id: string;
}

export interface UpdateCaseMutation {
  /**
   * Updates an existing case. Calling this without an existing
   * id will result in an error. The API does not take care of generating
   * ids. That has to be handled by the caller.
   */
  updateCase: UpdateCaseMutation_updateCase | null;
}

export interface UpdateCaseMutationVariables {
  case: UpdateCaseInput;
}
