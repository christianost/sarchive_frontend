/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: LoginMutation
// ====================================================

export interface LoginMutation {
  /**
   * The API is not public, so most queries and mutations
   * require you to send an authentication token, which has
   * to be retrieved by using the 'login' mutation.
   */
  login: string | null;
}

export interface LoginMutationVariables {
  email: string;
  password: string;
}
