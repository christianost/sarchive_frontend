import gql from 'graphql-tag'
import { LoginMutation, LoginMutationVariables } from '../graphql/LoginMutation'
import { FetchResult } from 'apollo-link'
import { storeToken } from '../../common/authentication'
import serverBus from '../../common/serverBus'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'

export const LOGIN_MUTATION = gql`
  mutation LoginMutation($email: String!, $password: String!) {
    login(username: $email, password: $password)
  }
`

export async function triggerLogin(
  apollo: DollarApollo<any>,
  username: string,
  password: string
): Promise<void> {
  const result = (await apollo.mutate<LoginMutation, LoginMutationVariables>({
    mutation: LOGIN_MUTATION,
    variables: { email: username, password: password }
  })) as FetchResult<LoginMutation>
  if (result && result.data && result.data.login && result.data.login.length > 0) {
    storeToken(result.data.login)
    serverBus.$emit('logged_in')
  } else {
    throw Error('Authentication failed')
  }
}
