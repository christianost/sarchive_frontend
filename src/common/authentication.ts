import client from './graphqlClient'
import { storageInstance } from './storage'

export const isAuthenticated = () => {
  const token = getToken()
  return token != null && token.length > 0
}

export const getToken = () => {
  return storageInstance.fetch('token')
}

export const storeToken = (token: string) => {
  storageInstance.save('token', token)
}

export const logout = () => {
  storageInstance.clear()
  client.clearStore()
}
