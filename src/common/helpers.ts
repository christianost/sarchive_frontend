export function filterGraphQlProperties(graphQlObject: any) {
  if (!graphQlObject) return {}
  var result = {}
  Object.keys(graphQlObject)
    .filter(propertyName => propertyName !== '__typename')
    .map(key => (result[key] = graphQlObject[key]))
  return result
}
