import { ApolloClient, DefaultOptions } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { onError } from 'apollo-link-error'
import { setContext } from 'apollo-link-context'
import { getToken } from './authentication'
import logger from './logger'
import serverBus from './serverBus'
import { ApolloLink } from 'apollo-link'

var backendUrl: string | undefined
if (process.env.NODE_ENV !== 'test') {
  backendUrl = process.env.VUE_APP_BACKEND_URL
  if (backendUrl == null || backendUrl.length === 0) {
    throw new Error('Environment variable VUE_APP_BACKEND_URL not provided')
  } else {
    logger.info(`Using backend at '${backendUrl}'`)
  }
}

// HTTP connection to the API
const httpLink = createHttpLink({
  // You should use an absolute URL here
  uri: backendUrl
})

const emitError = (error: string) => {
  logger.error(error)
  serverBus.$emit('error', error)
}

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors)
    graphQLErrors.forEach(({ message, locations, path }) =>
      emitError(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`)
    )
  if (networkError) {
    emitError(`[Network error]: ${networkError}`)

    if (networkError.hasOwnProperty('statusCode')) {
      // ServerError is not exported from apollo-link-error, hence statusCode is neither
      const statusCode = networkError['statusCode']
      logger.debug(`Network error status code is ${statusCode}`)
      if (statusCode === 401 || statusCode === 500) {
        serverBus.$emit('logged_out')
      }
    }
    if (networkError.toString().includes('Failed to fetch')) {
      logger.error('Network connection failed')
      serverBus.$emit('logged_out')
    }
  }
})

const authLink = setContext((_, { headers }) => {
  // get the authentication token to enrich the request
  const token = getToken()
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ''
    }
  }
})

// Cache implementation
const cache = new InMemoryCache()

// Create the apollo client
const client = new ApolloClient({
  link: ApolloLink.from([errorLink, authLink, httpLink]),
  cache: new InMemoryCache()
})

export default client
