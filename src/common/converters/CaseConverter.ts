import CaseFormInput from '../../models/CaseFormInput'
import { CreateCaseMutationVariables } from '../../components/Case/graphql/CreateCaseMutation'
import { CategoriesQuery_categories } from '../../components/Case/graphql/CategoriesQuery'
import {
  CasesQuery_cases,
  CasesQuery_cases_categories,
  CasesQuery_cases_categories_tags
} from '../../components/Cases/graphql/CasesQuery'
import { BoatType, SetCategoryInput } from '../../global-query-types'
import logger from '../logger'
import {
  combineDateTime,
  dateToBackendString,
  backendStringToDateString,
  backendStringToTimeString
} from './DateTimeConverter'
import cuid from 'cuid'

const toBoatType = (boatType: string) => {
  return (BoatType as any)[boatType]
}

const toInt = (num: number | undefined) => {
  return num ? Math.round(num) : 0
}

const toFloat = (num: number | undefined) => {
  return num ? parseFloat(num.toString()) : 0.0
}

export const convertCaseFormInput = (
  caseFormInput: CaseFormInput,
  categories: CategoriesQuery_categories[]
): CreateCaseMutationVariables => {
  const timestamp = dateToBackendString(
    combineDateTime(caseFormInput.creationDate!, caseFormInput.creationTime!)
  )

  const newCase: CreateCaseMutationVariables = {
    case: {
      caseNumber: caseFormInput.caseNumber!,
      timestamp: timestamp,
      boat: {
        type: toBoatType(caseFormInput.boatType!),
        color: caseFormInput.boatColor,
        engineStatus: caseFormInput.engineStatus ?? ''
      },
      peopleOnBoard: {
        women: toInt(caseFormInput.womenPob),
        men: toInt(caseFormInput.menPob),
        medical: toInt(caseFormInput.medicalPob),
        missing: toInt(caseFormInput.missingPob),
        drowned: toInt(caseFormInput.drownedPob),
        minors: toInt(caseFormInput.minorsPob),
        total: toInt(caseFormInput.totalPob)
      },
      freetext: caseFormInput.freetext
    }
  }

  if (caseFormInput.longitude != null && caseFormInput.latitude != null) {
    newCase.case.position = {
      longitude: toFloat(caseFormInput.longitude),
      latitude: toFloat(caseFormInput.latitude),
      timestamp: timestamp
    }
  }

  newCase.case.links =
    caseFormInput.link != null && caseFormInput.link!.length > 0 ? [caseFormInput.link!] : []

  newCase.case.categories = []
  if (caseFormInput.tags != null && caseFormInput.tags.length > 0) {
    const categoryId = categories != null && categories.length > 0 ? categories[0].id : '1'
    const categoryName = categories != null && categories.length > 0 ? categories[0].name : 'others'
    const tagCategory = <SetCategoryInput>{ id: categoryId, name: categoryName, tags: [] }
    // @ts-ignore - The tags control we use expects an array of {value:string} for its model and an array of string for the input field -.-
    caseFormInput.tags.map(t => tagCategory.tags!.push({ name: t['value'] }))
    newCase.case.categories.push(tagCategory)
  }

  logger.trace(`Converted form input to case with values '${JSON.stringify(newCase)}'`)

  return newCase
}

export const convertCaseToFormInput = (caseObject: CasesQuery_cases): CaseFormInput => {
  const formInput = <CaseFormInput>{
    caseNumber: caseObject.caseNumber,
    creationDate: backendStringToDateString(caseObject.timestamp!),
    creationTime: backendStringToTimeString(caseObject.timestamp!),
    freetext: caseObject.freetext,
    boatType: caseObject.boat?.type,
    boatColor: caseObject.boat?.color,
    engineStatus: caseObject.boat?.engineStatus,
    womenPob: caseObject.peopleOnBoard?.women,
    menPob: caseObject.peopleOnBoard?.men,
    minorsPob: caseObject.peopleOnBoard?.minors,
    drownedPob: caseObject.peopleOnBoard?.drowned,
    missingPob: caseObject.peopleOnBoard?.missing,
    medicalPob: caseObject.peopleOnBoard?.medical,
    totalPob: caseObject.peopleOnBoard?.total,
    tags:
      caseObject.categories != null && caseObject.categories.length > 0
        ? caseObject.categories
            .map((c: CasesQuery_cases_categories | null) =>
              c != null && c.tags != null && c.tags.length > 0
                ? c.tags.map((t: CasesQuery_cases_categories_tags) => {
                    return { value: t.name }
                  })
                : []
            )
            .reduce((previous: any[], current: any[]) =>
              previous != null ? previous.concat(current) : current
            )
        : []
  }
  logger.trace(`Converted case to form input with values '${JSON.stringify(formInput)}' `)
  return formInput
}

export const determineTotalPob = (caseObject: CasesQuery_cases): number => {
  const pob = caseObject.peopleOnBoard
  if (!pob) return 0
  // It's valid to assume that all properties are not null if the object is poulated in the service response object
  return pob.total! !== 0 ? pob.total! : pob.women! + pob.men! + pob.minors!
}
