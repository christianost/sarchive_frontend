export function combineDateTime(dateString: string, timeString: string): Date {
  return new Date(dateString + ' ' + timeString)
}

export function combineDateTimeToBackendString(dateString: string, timeString: string): string {
  return dateToBackendString(combineDateTime(dateString, timeString))
}

function padDigits(monthOrDate: number) {
  return monthOrDate.toString().padStart(2, '0')
}

export function dateToBackendString(date: Date): string {
  return `${dateToDateString(date)} ${dateToTimeString(date)}`
}

export function backendStringToDateString(backendDateTimeString: string): string {
  return dateToDateString(new Date(backendDateTimeString))
}

export function dateToDateString(date: Date): string {
  return `${date.getFullYear()}-${padDigits(date.getMonth() + 1)}-${padDigits(date.getDate())}`
}

export function backendStringToTimeString(backendDateTimeString: string): string {
  return dateToTimeString(new Date(backendDateTimeString))
}

export function dateToTimeString(date: Date): string {
  return `${padDigits(date.getHours())}:${padDigits(date.getMinutes())}:${padDigits(
    date.getSeconds()
  )}`
}
