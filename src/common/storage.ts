import logger from './logger'

const caseDbPrefix = 'casedb-'
export const keyAccessToken = 'access_token'
export const keyCases = 'cases'
export class Storage {
  fetch(key: string) {
    logger.debug('Reading value for key ' + key)
    return localStorage.getItem(caseDbPrefix + key)
  }
  save(key: string, value: any) {
    logger.debug('Saving value for key ' + key)
    localStorage.setItem(
      caseDbPrefix + key,
      typeof value !== 'string' ? JSON.stringify(value) : value
    )
  }
  clear() {
    localStorage.clear()
  }
}
export const storageInstance = new Storage()
