interface Date {
  toISODateString(): string
  toShortTimeString(): string
}

Date.prototype.toISODateString = function(this: Date) {
  return (
    this.getFullYear() +
    '-' +
    this.getMonth()
      .toString()
      .padStart(2, '0') +
    '-' +
    this.getDate()
      .toString()
      .padStart(2, '0')
  )
}

Date.prototype.toShortTimeString = function(this: Date) {
  return this.toLocaleTimeString().slice(0, 5)
}
