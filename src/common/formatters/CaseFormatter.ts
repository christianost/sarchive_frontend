export default class CaseFormatter {
  ensureFirstLetterUpper(value: string) {
    return value.charAt(0).toUpperCase() + value.slice(1)
  }
}
