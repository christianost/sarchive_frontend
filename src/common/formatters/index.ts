/* istanbul ignore file */

import GeoLocationFormatter from './GeoLocationFormatter'
import CaseFormatter from './CaseFormatter'

export const formatLatLng = (value: number) => new GeoLocationFormatter().format(value)
export const formatFirstUp = (value: string) => new CaseFormatter().ensureFirstLetterUpper(value)
