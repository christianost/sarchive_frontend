const LON_LAT_DIGITS = 6

export default class GeoLocationFormatter {
  format(value: number): string {
    return value.toFixed(LON_LAT_DIGITS)
  }
}
