enum LogLevel {
  FATAL = 'FATAL',
  ERROR = 'ERROR',
  WARN = 'WARN',
  INFO = 'INFO',
  DEBUG = 'DEBUG',
  TRACE = 'TRACE'
}

const createMessage = (level: LogLevel, message: string) => {
  const now = new Date()
  return (
    now.getHours() +
    ':' +
    now
      .getMinutes()
      .toString()
      .padStart(2, '0') +
    ':' +
    now
      .getSeconds()
      .toString()
      .padStart(2, '0') +
    '|' +
    level +
    '|' +
    message
  )
}

const writeToLog = (message: string) => {
  console.log(message)
}

const writeToErrorLog = (message: string) => {
  console.error(message)
}

const logger = {
  fatal: (error: Error) => {
    writeToErrorLog(
      createMessage(LogLevel.FATAL, error.name + '\n' + error.message + '\n' + error.stack)
    )
  },
  error: (error: Error | string) => {
    if (error instanceof Error) {
      writeToErrorLog(
        createMessage(LogLevel.ERROR, error.name + '\n' + error.message + '\n' + error.stack)
      )
    } else {
      writeToLog(createMessage(LogLevel.ERROR, error))
    }
  },
  warn: (message: string) => {
    writeToLog(createMessage(LogLevel.WARN, message))
  },
  info: (message: string) => {
    writeToLog(createMessage(LogLevel.INFO, message))
  },
  debug: (message: string) => {
    writeToLog(createMessage(LogLevel.DEBUG, message))
  },
  trace: (message: string) => {
    writeToLog(createMessage(LogLevel.TRACE, message))
  }
}

export default logger
