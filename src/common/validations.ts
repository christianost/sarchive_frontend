import { extend } from 'vee-validate'
import { required, min } from 'vee-validate/dist/rules'

extend('required', { ...required, message: 'This field is required' })
extend('min', { ...min, message: 'Values does not meet minimal length' })
extend('integer', {
  validate: value => isInteger(value),
  message: 'Only integer values allowed'
})
extend('float', {
  validate: value => isFloat(value),
  message: "Only numbers and decimal '.' allowed"
})

export const isInteger = (value: string) => /^-?\d*$/.test(value)
export const isFloat = (value: string) => /^-?\d*(\.\d+)?$/.test(value)
