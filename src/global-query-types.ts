/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum BoatType {
  METAL = "METAL",
  RUBBER = "RUBBER",
  WOOD = "WOOD",
}

export interface BoatInput {
  type?: BoatType | null;
  color?: string | null;
  engineStatus?: string | null;
}

export interface CreateCaseInput {
  caseNumber: string;
  freetext?: string | null;
  peopleOnBoard?: PeopleOnBoardInput | null;
  boat?: BoatInput | null;
  position?: PositionInput | null;
  links?: (string | null)[] | null;
  categories?: (SetCategoryInput | null)[] | null;
  timestamp?: string | null;
}

export interface PeopleOnBoardInput {
  women?: number | null;
  men?: number | null;
  minors?: number | null;
  total?: number | null;
  medical?: number | null;
  missing?: number | null;
  drowned?: number | null;
}

export interface PositionInput {
  latitude: number;
  longitude: number;
  timestamp?: string | null;
}

export interface SetCategoryInput {
  id: string;
  name?: string | null;
  tags?: TagInput[] | null;
}

export interface TagInput {
  name: string;
}

export interface UpdateCaseInput {
  id: string;
  caseNumber: string;
  freetext?: string | null;
  peopleOnBoard?: PeopleOnBoardInput | null;
  boat?: BoatInput | null;
  links?: (string | null)[] | null;
  categories?: (SetCategoryInput | null)[] | null;
  timestamp?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
