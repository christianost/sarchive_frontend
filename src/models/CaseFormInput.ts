export default interface CaseFormInput {
  caseNumber?: string
  creationDate?: string
  creationTime?: string
  womenPob?: number
  menPob?: number
  minorsPob?: number
  medicalPob?: number
  missingPob?: number
  drownedPob?: number
  totalPob?: number
  boatType?: string
  boatColor?: string
  engineStatus?: string
  link?: string
  tags?: string[] | { value: string }[]
  longitude?: number
  latitude?: number
  freetext?: string
}
