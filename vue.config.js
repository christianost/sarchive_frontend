module.exports = {
  outputDir: 'dist',
  configureWebpack: {
    devtool: 'source-map'
  }
}
