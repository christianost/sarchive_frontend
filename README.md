# Historical Database

## Project Description

A civil maritime rescue coordination center (cMRCC) - is a platform providing a GIS, a database and a communication infrastructure for NGOs who are operating in the field of Search and Rescue in the Mediterranean.

The Historical Database is the place where data about cases (search and rescue events, distress cases) will get stored. The data input will be possible via the OneFleet application or directly via an interface at the Historical Database.
### Input via OneFleet application:
For open distress case which got put in and coordinated via the OneFleet app there will be a possibility to transfer them at a specific point to the Historical Database. This specific point needs to be discussed and defined, most likely it will be at the moment of a finished rescue.

### Input directly via Historical Database
The Historical Database will provide a way to input data about closed distress cases directly. This would be applicable for cases where there was no direct involvment or coordination of any SAR NGOs.

The data shall be used for internal statistics and research as well as potentially be (partly) opened to researchers, journalistis, ...
Therefore powerful options to search and display the data will be needed.

## Technical Development

At the moment, the following steps are included in the development plan:

We use VueJS with Bootstrap as UI-framework in order to facilitate easy prototyping and use TypeScript as the programming language to benefit from type-safety. The frontend uses GraphQL to talk to the respective backend service.

The code merged to `master` is deployed to github pages at <https://niczem.gitlab.io/casesdb/>.

This frontend directly talks to the backend service ([repository here][https://gitlab.com/domma/cases_backend]), hosted on heroku at <http://casesdb.herokuapp.com>.

## About this Repo

**Isn't this the paragraph about the OneFleet?**
This repo contains all the services that you need to run to track and display vehicles and positions. It contains the main application which displays vehicles and cases on a map, the database in which locations, vehicles and positions are stored and the location-service which gathers e.g. ship locations to store them in the database. *The location-service and the database have to run when you start the app*

## Contributing

See our [Code of Conduct](CODE_OF_CONDUCT.md).

### Issues

Please report all issues in the [issue page](https://gitlab.com/niczem/casesdb_frontend/issues)

## Developer Guide

### Project setup

```sh
npm install
```

### Development Mode

Run app with watch task via

```sh
npm start
```

The TypeScript compiler will display compilation errors on command line.
You should see this output if there are no errors

```sh
DONE  Compiled successfully in 4161ms                                                                                                                                                                9:30:06 AM

No type errors found
Version: typescript 3.5.3
Time: 2354ms

  App running at:
  - Local:   http://localhost:8080/ 
  - Network: http://192.168.178.91:8080/

  Note that the development build is not optimized.
  To create a production build, run npm run build.


```

You can debug using VSCode and the Chrome debugger plugin using the configuration in `.vscode/launch.json`.

### Compiles and minifies for production

```sh
npm run build
```

### Run your unit tests

```sh
npm run test:unit
```

### Lints and fixes files

```sh
npm run lint
```

## Development Workflow

We use Kanban, where the issues on the frontend project (this one) define the next item(s) to work on, while the [backend](https://gitlab.com/civilmrcc/sarchive_backend) needs to follow.

Check this [board](https://gitlab.com/civilmrcc/sarchive_frontend/-/boards) for an overview.

Important cornerstones of the workflow are:
- label issue with `To Do` if you want to raise it's priority and have a developer pick it "next"
- label issue as `Doing` and assign yourself when you are working on it, to avoid two people working on the same
- label issue with `backend` if there is a dependency in the backend project and link the backend issue
- To be transparent about changes and deployment status:
  - work on issues in `feature/*` or `fix/` branches (see [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow))
  - label issue as `done`, when developments/changes are done and merged to the `develop` branch
  - close issues which are `done`, when the respective changes was merged from `develop` to `master`
    - this is done at an aligned point in time via merge request, by a maintainer of the repository/ies
  - The state of `master` will automatically be deployed to testing

## HOW-TOs

### Attach to Chrome for debugging

* Install the `Debugger for Chrome` extension for VSCode/VSCodium
* Run the app via `npm start`
* Launch Chrome with debugger enabled via `F5` (see `.vscode/launch.json`)
* Debug!

### Debug Vue.JS control state

Use the Firefox (or Chrome) Vue.js devtools plugin: https://addons.mozilla.org/en-US/firefox/addon/vue-js-devtools/

### How to test with my local checkout of `cases_backend`

Change the target URL in the `./common/graphqlClient.ts` module to `http://localhost:<PORT>`

### How to generate new types for the GraphQL API?

1. Define the `gql` query or mutation in your vue component or a dedicated file
2. Run `npm run codegen`
3. The apollo-client code generator will generate the types in `<folder_of_the_query>/graphql/<query_name>.ts`

### How to deploy to the testing server

Similarly to the process documented in the [onefleet README](https://gitlab.com/civilmrcc/onefleet):
- login to the server via ssh
- clone this repository to your home folder
- export the database url for the backend via `export DATABASE_URL=<url>`, where the url has the format `postgres://user:password@host:port/dbname`
- run `sudo -E ./deploy.sh` to pull the frontend and backend images and run the stack based on the compose file
- to have watchtower pull new images and deploy them, run `sudo -E docker run -d --name watchtower-sarchive -v /var/run/docker.sock:/var/run/docker.sock containrrr/watchtower sarchive_frontend sarchive_backend`

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### 

## FAQ

Where do I find type definitions for JS libraries? --> <https://github.com/DefinitelyTyped/DefinitelyTyped>

## Additional documentation

See docs for `eslint-plugin-vue` [LINK](https://eslint.vuejs.org/user-guide/#usage) and this [blog](https://alligator.io/vuejs/eslint-vue-vetur/).
